#!/bin/sh
#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

set -e

SCRIPT_NAME="$(basename "$0")"
KLUS_SCRIPT="$(readlink -m "$0")"
KLUS_SCRIPT_NAME="$(basename "$KLUS_SCRIPT")"
KLUS_SCRIPT_DIR="$(dirname "$KLUS_SCRIPT")"

accept_klus ()
{
    case "$1" in
        setup|setup.sh)
            echo -n run_klus_script klus/setup.sh
        ;;
        cleanup|cleanup.sh)
            echo -n run_klus_script klus/cleanup.sh
        ;;
        kdesrc-build|kdesrc-build.sh)
            echo -n run_klus_script klus/kdesrc-build.sh
        ;;
        arc|arcanist|arc.sh|arcanist.sh)
            echo -n run_klus_script klus/arcanist.sh
        ;;
        apk|apk.sh|android-sdk|android-sdk.sh)
            echo -n run_klus_script klus/apk.sh
        ;;
        cmake|cmake.sh)
            echo -n run_klus_script klus/cmake.sh
        ;;
        flatpak|flatpak-builder|flatpak.sh|flatpak-builder.sh)
            echo -n run_klus_script klus/flatpak.sh
        ;;
        test|test.sh)
            echo -n run_klus_script klus/test.sh
        ;;
        shell|shell.sh)
            echo -n run_klus_script klus/shell.sh
        ;;
        -h|--help|help)
            echo -n klus_help
        ;;
        *)
            false
        ;;
    esac
}

klus_help ()
{
    cat << KLUS_USAGE
Klus is a collection of (opinionated) scripts that help with KDE workflows

Usage:

$KLUS_SCRIPT_NAME -h|--help           : display this 'help'
$KLUS_SCRIPT_NAME <command> [options] : run a Klus
$KLUS_SCRIPT_NAME <command> --help    : get help with a Klus

Commands:

  apk|android-sdk  : runs the KDE Android SDK to build an app
  arc|arcanist     : runs arcanist using a container
  cleanup          : (partial) clean up of a Klus setup
  cmake            : configure a cmake build directory with some predefined defaults
  flatpak-builder  : build KDE flatpaks
  help             : alias for $KLUS_SCRIPT_NAME --help
  kdesrc-build     : runs kdesrc-build using a container
  setup            : (first) time setup
  test             : check if Klus has been setup for this directory yet
KLUS_USAGE
}

klus_not_implemented_yet ()
{
    local command="$1"
    echo "This command is not implemented yet: $command" >&2
    echo "Try: $KLUS_SCRIPT_NAME --help"
    exit 255
}

run_klus_script ()
{
    local klus_script="$KLUS_SCRIPT_DIR/$1"
    local command="$2"

    if [ -f "$klus_script" ]
    then
        shift 2
        . "$klus_script"
        klus "$@"
    else
        echo "Invalid klus script: $klus_script" >&2
        exit 253
    fi
}

invalid_klus ()
{
    local command="$1"
    if [ -z "$command" ]
    then
        echo "A command is required" >&2
    else
        echo "Invalid command: $command" >&2
    fi
    klus_help >&2
    exit 254
}

KLUS=""
if KLUS="$(accept_klus "$SCRIPT_NAME")"
then
    KLUS_USAGE_SCRIPT_PREAMBLE="$SCRIPT_NAME"
    $KLUS "$SCRIPT_NAME" "$@"
else
    case "$SCRIPT_NAME" in
        klus|klus.sh)
            if KLUS="$(accept_klus "$1")"
            then
                KLUS_USAGE_SCRIPT_PREAMBLE="$SCRIPT_NAME $1"
                $KLUS "$@"
            else
                invalid_klus "$1"
            fi
        ;;
        *)
            invalid_klus "$0"
        ;;
    esac
fi
