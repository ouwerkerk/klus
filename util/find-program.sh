#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

test_for_required_klus_prog ()
{
    local prog="$1"
    local path=""
    if path="$(which "$prog" 2>/dev/null)" && \
        [ -n "$path" ] && \
        [ -n "$KLUS_SCRIPT" ] && \
        path="$(readlink -m "$path")" && \
        [ "$path" != "$KLUS_SCRIPT" ]
    then
        return 0
    else
        return 1
    fi
}

klus_programs_are_missing ()
{
    local failed=""
    echo "Checking for required programs: $@"
    for prog in $@
    do
        if test_for_required_klus_prog "$prog"
        then
            : # nothing to do
        else
            failed="$prog"
            echo "Unable to find: $prog" >&2
        fi
    done

    if [ -n "$failed" ]
    then
        return 0
    else
        return 1
    fi
}
