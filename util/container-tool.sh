#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

KLUS_CONTAINER_TOOL=""
KLUS_REBUILD_CONTAINER_TOOL=""
KLUS_CONTAINER_TOOL_ARGS=""
KLUS_CONTAINER_TOOL_AGAIN=""
KLUS_CONTAINER_TOOL_WIPE=""
KLUS_CONTAINER_TOOL_MOUNTS=""
KLUS_CONTAINER_TOOL_MODE="default"
KLUS_CONTAINER_TOOL_VARIANT=""

klus_container_tool_usage ()
{
    cat << USAGE
$KLUS_USAGE_SCRIPT_PREAMBLE : builds and runs $KLUS_CONTAINER_TOOL docker images.

Getting help:

 $KLUS_USAGE_SCRIPT_PREAMBLE -h|--help
 $KLUS_USAGE_SCRIPT_PREAMBLE <variant> --help # for $KLUS_CONTAINER_TOOL help

Running $KLUS_CONTAINER_TOOL:

 $KLUS_USAGE_SCRIPT_PREAMBLE [options] <variant> [$KLUS_CONTAINER_TOOL args]

General utilities:

 $KLUS_USAGE_SCRIPT_PREAMBLE -l|--ls|--list # see available variants
 $KLUS_USAGE_SCRIPT_PREAMBLE -w|--wipe # remove (remaining) images for $KLUS_CONTAINER_TOOL

Options:

 -h, --help                          : display this 'help' instead
 -l, -ls, --list                     : display a list of available image variants instead
 -s, -sh, --shell <variant>          : enter a shell (/bin/sh) inside the container instead
 -b, --build, --build-only <variant> : build the container and exit
 -a, --again                         : rebuild the container image even if it already exists
 -w, --wipe                          : remove any container images for $KLUS_CONTAINER_TOOL

USAGE
}

klus_build_container_tool_command ()
{
    # some tools like qmlplugindump complain if XDG_RUNTIME_DIR is not set or does not point to a valid (?) directory
    # which is why the containers include a dummy directory for it in the image.
    if [ -z "$XDG_RUNTIME_DIR" ]
    then
        XDG_RUNTIME_DIR="/run/user/$(id -u)"
    fi

    cat << CONTAINER_BUILD_SCRIPT
    buildah bud --layers=false \
        --build-arg KDE_SRC_DIR="$KDE_SRC_DIR" \
        --build-arg KDE_DIST_DIR="$KDE_DIST_DIR" \
        --build-arg HOME_DIR="$HOME" \
        --build-arg XDG_RUNTIME_DIR="$XDG_RUNTIME_DIR" \
        -t "$KLUS_CONTAINER_TOOL:$KLUS_CONTAINER_TOOL_VARIANT" \
        - < "$KLUS_SCRIPT_DIR/docker/$KLUS_CONTAINER_TOOL/$KLUS_CONTAINER_TOOL_VARIANT.docker"
CONTAINER_BUILD_SCRIPT
}

klus_run_container_tool_command ()
{
    local entrypoint="$1"

    # inject $KDE_DIR/share into the default data search path. Things like QStandardPaths should pick up on this.
    local datadirs="$KDE_DIST_DIR/share"
    if [ -n "$XDG_DATA_DIRS" ]
    then
        datadirs="$datadirs:$XDG_DATA_DIRS"
    fi


    local memlimit=""
    if [ -n "$CONTAINER_MEMORY_LIMIT" ]
    then
        memlimit="--memory-swap \"$CONTAINER_MEMORY_LIMIT\" --memory \"$CONTAINER_MEMORY_LIMIT\""
    fi

    local buildflags=""
    if [ -n "$DEFAULT_JOB_COUNT" ]
    then
        buildflags="-e \"MAKEFLAGS=-j $DEFAULT_JOB_COUNT\" -e \"NINJAFLAGS=-j $DEFAULT_JOB_COUNT\""
    fi

    #
    # Make sure to 'regenerate' these directories if deleted as part of cleanup.
    #
    mkdir -p "$KDE_SRC_DIR"
    mkdir -p "$KDE_DIST_DIR"

    #
    # Run the tool inside the container
    #
    #  - as the user that runs this script; so generated output will be available to the user without requiring sudo/su
    #  - mapping a fixed source directory
    #  - mapping a fixed destination/dist directory to 'make install' to
    #  - set up environment variables for Qt tools
    #  - pass arguments to this script on: this lets the user override CMD defaults from the docker image directly.
    #    i.e. it lets the user pass options to kdesrc-build running inside the container.
    #
    cat << CONTAINER_TOOL_COMMAND
        podman run -it --rm \
            $memlimit \
            -v "$KDE_SRC_DIR:$KDE_SRC_DIR:rw" \
            -v "$KDE_DIST_DIR:$KDE_DIST_DIR:rw" \
            $($KLUS_CONTAINER_TOOL_MOUNTS) \
            $buildflags \
            -e "XDG_DATA_DIRS=$datadirs" -e "WORK_DIR=$(pwd)" \
            $entrypoint \
            "$KLUS_CONTAINER_TOOL:$KLUS_CONTAINER_TOOL_VARIANT"
CONTAINER_TOOL_COMMAND
}

klus_pick_container_tool_variant ()
{
    local variant="$1"
    if [ -z "$variant" ]
    then
        echo "A valid variant name is required" >&2
        return 1
    elif [ -f "$KLUS_SCRIPT_DIR/docker/$KLUS_CONTAINER_TOOL/$variant.docker" ]
    then
        KLUS_CONTAINER_TOOL_VARIANT="$variant"
    else
        echo "Not a valid variant name: '$variant'" >&2
        return 1
    fi
}

klus_list_container_tool_variants ()
{
    for variant in "$KLUS_SCRIPT_DIR/docker/$KLUS_CONTAINER_TOOL/"*.docker
    do
        if [ -f "$variant" ]
        then
            echo "$(basename "$variant" .docker)"
        fi
    done
}


save_arg_for_container_klus ()
{
    if [ -n "$KLUS_CONTAINER_TOOL_ARGS" ]
    then
        KLUS_CONTAINER_TOOL_ARGS="$KLUS_CONTAINER_TOOL_ARGS $1"
    else
        KLUS_CONTAINER_TOOL_ARGS="$1"
    fi
}

klus_scan_for_container_tool_options ()
{
    local tool="$1"
    shift 1

    KLUS_CONTAINER_TOOL_VARIANT=""
    KLUS_OPTS_STOP=""
    while [ $# -ne 0 ]
    do
        if [ -z "$KLUS_OPTS_STOP" ]
        then
            case "$1" in
            -l|-ls|--list)
                KLUS_CONTAINER_TOOL_MODE="list"
            ;;
            -b|--build|--build-only)
                KLUS_CONTAINER_TOOL_MODE="build"
                shift
                klus_pick_container_tool_variant "$1" || return 1
            ;;
            -s|-sh|--shell)
                KLUS_CONTAINER_TOOL_MODE="shell"
                shift
                klus_pick_container_tool_variant "$1" || return 1
            ;;
            -w|--wipe)
                if [ -z "$KLUS_CONTAINER_TOOL_AGAIN" -a -z "$KLUS_CONTAINER_TOOL_WIPE" ]
                then
                    KLUS_CONTAINER_TOOL_WIPE="true"
                else
                    echo "Duplicate/conflicting option: $1" >&2
                    return 1
                fi
            ;;
            -a|--again)
                if [ -z "$KLUS_CONTAINER_TOOL_AGAIN" -a -z "$KLUS_CONTAINER_TOOL_WIPE" ]
                then
                    KLUS_CONTAINER_TOOL_AGAIN="true"
                else
                    echo "Duplicate/conflicting option: $1" >&2
                    return 1
                fi
            ;;
            *)
                if [ -z "$KLUS_CONTAINER_TOOL_VARIANT" ]
                then
                    KLUS_OPTS_STOP="true"
                    klus_pick_container_tool_variant "$1"
                else
                    save_arg_for_container_klus "$1"
                fi
            ;;
            esac
        else
            save_arg_for_container_klus "$1"
        fi
        shift 1
    done
}

list_klus_container_images ()
{
    buildah images -n  --format "{{.ID}}" --filter "label=klus.tool=$KLUS_CONTAINER_TOOL"
}

wipe_klus_container_images ()
{
    echo "Wiping all container images for: $KLUS_CONTAINER_TOOL"
    list_klus_container_images | while read -r image || [ -n "$image" ]
    do
        if [ -n "$image" ]
        then
            buildah rmi "$image"
        fi
    done
}

auto_detect_klus_container_image ()
{
    buildah images -n  --format "{{.ID}}" \
        --filter "label=klus.tool=$KLUS_CONTAINER_TOOL" \
        --filter "label=klus.variant=$KLUS_CONTAINER_TOOL_VARIANT"
}

try_container_tool_klus ()
{
    local klusrc="$1"

    . "$KLUS_SCRIPT_DIR/util/find-program.sh"

    if klus_programs_are_missing buildah podman
    then
        echo "Missing required programs: cowardly refusing to run $KLUS_CONTAINER_TOOL" >&2
        exit 201
    fi

    echo "Loading existing klusrc: $klusrc"
    . "$klusrc"

    if [ -n "$KLUS_CONTAINER_TOOL_WIPE" ]
    then
        wipe_klus_container_images
    fi

    local klus_container_image_id="$(auto_detect_klus_container_image)"
    if [ -z "$klus_container_image_id" ]
    then
        echo "No image detected: building image for: $KLUS_CONTAINER_TOOL ..."
        local build_command="$(klus_build_container_tool_command)"
        (
            set -x
            eval $build_command
        )
    elif [ -z "$KLUS_CONTAINER_TOOL_AGAIN" ]
    then
        echo "Found $klus_container_image_id, skipping build for: $KLUS_CONTAINER_TOOL"
    else
        echo "Rebuilding image for: $KLUS_CONTAINER_TOOL ..."
        local build_command="$(klus_build_container_tool_command)"
        (
            set -x
            buildah rmi "$klus_container_image_id"
            eval $build_command
        )
    fi

    case "$KLUS_CONTAINER_TOOL_MODE" in
        shell)
            echo "Entering shell in container for: $KLUS_CONTAINER_TOOL ..."
            local tool_command="$(klus_run_container_tool_command "--entrypoint /bin/sh")"
            (
                set -x
                eval $tool_command
            )
        ;;
        default)
            echo "Running $KLUS_CONTAINER_TOOL ..."
            local tool_command="$(klus_run_container_tool_command "")"
            (
                set -x
                eval $tool_command $KLUS_CONTAINER_TOOL_ARGS
            )
        ;;
    esac
}

klus_container_tool_setup_required ()
{
    local klusdir="$1"
    echo "No klusrc was detected" >&2
    echo "It looks like Klus is not yet set up for this directory: $klusdir" >&2
    echo "Cowardly refusing to run $KLUS_CONTAINER_TOOL" >&2
    exit 200
}

container_tool_klus ()
{
    KLUS_CONTAINER_TOOL="$1"
    KLUS_CONTAINER_TOOL_MOUNTS="$2"
    shift 2

    case "$1" in
        -h|--help)
            klus_container_tool_usage "$KLUS_CONTAINER_TOOL"
            exit 0
        ;;
        *)
            if klus_scan_for_container_tool_options "$KLUS_CONTAINER_TOOL" "$@"
            then
                case "$KLUS_CONTAINER_TOOL_MODE" in
                    help)
                        klus_container_tool_usage "$KLUS_CONTAINER_TOOL"
                    ;;
                    list)
                        klus_list_container_tool_variants "$KLUS_CONTAINER_TOOL"
                    ;;
                    shell|build|default)
                        if [ -z "$KLUS_CONTAINER_TOOL_VARIANT" -a -z "$KLUS_CONTAINER_TOOL_WIPE" ]
                        then
                            echo "No variant selected for $KLUS_CONTAINER_TOOL" >&2
                            echo "To see all variants use: $KLUS_USAGE_SCRIPT_PREAMBLE --list" >&2
                            klus_container_tool_usage "$KLUS_CONTAINER_TOOL" >&2
                            exit 200
                        elif [ -z "$KLUS_CONTAINER_TOOL_VARIANT" ]
                        then
                            wipe_klus_container_images
                        else
                            . "$KLUS_SCRIPT_DIR/util/find-klusrc.sh"
                            find_klus_rc try_container_tool_klus klus_container_tool_setup_required
                        fi
                    ;;
                    *)
                        echo "Tool mode not implemented (yet): $KLUS_CONTAINER_TOOL_MODE" >&2
                        exit 254
                    ;;
                esac
            else
                klus_container_tool_usage "$KLUS_CONTAINER_TOOL" >&2
                exit 200
            fi
        ;;
    esac
}
