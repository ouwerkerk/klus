#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

fits_ceiling ()
{
    local current="$1"
    local ceiling="$2"

    case "$ceiling" in
        "$current/"*)
            false
        ;;
        *)
            test -n "$current" -a "$current" != "$ceiling" -a "$current" != "/"
        ;;
    esac
}

fits_klus_ceiling ()
{
    local current="$1"
    local ceilings="$2"

    while [ -n "$ceilings" ]
    do
        local ceiling="${ceilings%%:*}"
        fits_ceiling "$current" "$ceiling" || return 1
        ceilings="${ceilings#$ceiling}"
        ceilings="${ceilings#:}"
    done
}

klus_ceiling_dirs ()
{
    local ceiling_dirs="/"
    if [ -n "$KLUS_CEILING_DIRECTORIES" ]
    then
        local ceilings="$KLUS_CEILING_DIRECTORIES"
        while [ -n "$ceilings" ]
        do
            local ceiling="${ceilings%%:*}"
            if [ -n "$ceiling" ]
            then
                local entry="$(readlink -m "$ceiling")"
                if [ "$entry" != "/" ]
                then
                    ceiling_dirs="$ceiling_dirs:$entry"
                fi
                ceilings="${ceilings#$ceiling}"
            fi
            ceilings="${ceilings#:}"
        done
    else
        local entry="$(readlink -m "$HOME")"
        if [ "$entry" != "/" ]
        then
            ceiling_dirs="$ceiling_dirs:$entry"
        fi
    fi
    echo -n "$ceiling_dirs"
}

find_klus_rc_in ()
{
    local d="$1"
    local ceilings="$2"
    local klus_rc="$d/klusrc"
    if [ -f "$klus_rc" -a -r "$klus_rc" ]
    then
        echo -n "$klus_rc"
    elif fits_klus_ceiling "$d" "$ceilings"
    then
        find_klus_rc_in "$(readlink -m "$d/..")" "$ceilings"
    else
        return 1
    fi
}

find_klus_rc ()
{
    local if_ok="$1"
    local if_error="$2"
    local rc=""
    local d="$(pwd)"
    if rc="$(find_klus_rc_in "$(readlink -m "$d")" "$(klus_ceiling_dirs)")"
    then
        $if_ok "$rc" "$d"
    else
        $if_error "$d"
    fi
}
