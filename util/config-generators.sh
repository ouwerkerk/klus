#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

default_klus_job_count ()
{
    local cores="$(nproc --all)"
    local jobs="$(($cores * 5 / 4))"
    echo -n "$jobs"
}

generate_klusrc ()
{
    local file="$1"
    echo
    tee "$file" << KLUSRC
KDE_SRC_DIR="$KDE_SRC_DIR"
KDE_DIST_DIR="$KDE_DIST_DIR"
KDESRC_BUILD_DIR="$KDESRC_BUILD_DIR"

KDESRC_BUILD_RC="$KDESRC_BUILD_RC"
ARC_RC="$ARC_RC"
GIT_CONFIG="$GIT_CONFIG"
SSH_CONFIG="$SSH_CONFIG"
SSH_HOSTS="$SSH_HOSTS"
KDE_IDENTITY="$KDE_IDENTITY"

# CONTAINER_MEMORY_LIMIT="$CONTAINER_MEMORY_LIMIT" #TODO: check this limit makes sense on your machine
DEFAULT_JOB_COUNT="$(default_klus_job_count)"
KLUSRC
    echo
}

generate_gitconfig ()
{
    local file="$1"
    echo
    tee "$file" << GIT
[user]
name = $KLUS_GIT_USER
email = $KLUS_GIT_EMAIL

[url "https://invent.kde.org/"]
insteadOf = kde:
[url "ssh://git@invent.kde.org/"]
pushInsteadOf = kde:
GIT
    echo
}

generate_arc_rc ()
{
    local file="$1"
    echo "{}" > "$file"
    chmod 600 "$file"
}

generate_ssh_hosts ()
{
    local file="$1"
    touch "$file"
}

generate_ssh_config ()
{
    local file="$1"
    echo
    tee "$file" << SSH
Host *.kde.org
        User git
        IdentityFile ~/.ssh/kde_identity
SSH
    echo
}

generate_kdesrc_build_rc ()
{
    local file="$1"
    local jobs="$(default_klus_job_count)"
    echo
    tee "$file" << CFG

# This is a sample kdesrc-build configuration file appropriate for KDE
# Frameworks 5-based build environments.
#
# See the kdesrc-buildrc-sample for explanations of what the options do, or
# view the manpage or kdesrc-build documentation at
# https://docs.kde.org/trunk5/en/extragear-utils/kdesrc-build/index.html
global
    git-user $KLUS_GIT_USER <$KLUS_GIT_EMAIL>
    git-desired-protocol git
    branch-group kf5-qt5

    # Where to download source code. By default the build directory and
    # logs will be kept under this directory as well.
    source-dir $KDE_SRC_DIR
    kdedir $KDE_DIST_DIR
    qtdir $KDE_DIST_DIR

    cmake-options -DCMAKE_BUILD_TYPE=RelWithDebInfo
    cmake-generator Ninja
    num-cores $jobs
    num-cores-low-mem $jobs
    make-options -j$jobs
end global

# Instead of specifying modules here, the current best practice is to refer to
# KF5 module lists maintained with kdesrc-build by the KF5 developers. As new
# modules are added or modified, the kdesrc-build KF5 module list is altered to
# suit, and when you update kdesrc-build you will automatically pick up the
# needed changes.

# NOTE: You MUST change the path below to include the actual path to your
# kdesrc-build installation.
# Inside the container(s) this will be at /kdesrc-build/bin
# Do not change if unsure
include /kdesrc-build/bin/kf5-qt5-build-include
include /kdesrc-build/bin/qt5-build-include
include /kdesrc-build/bin/custom-qt5-libs-build-include

# If you wish to maintain the module list yourself that is possible, simply
# look at the files pointed to above and use the "module-set" declarations that
# they use, with your own changes.

# It is possible to change the options for modules loaded from the file
# included above (since it's not possible to add a module that's already been
# included), e.g.

options gpgme
    # work around: add libgpg-error include path manually
    set-env CFLAGS -I$KDE_DIST_DIR/include
end options
CFG
    echo
}
