#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

klus_generate_config ()
{
    local type="$1"
    local file="$2"
    local generator="$3"
    local fn="$4"
    local overwrite="$5"
    local todos="$6"

    if [ -f "$file" -a -z "$overwrite" ]
    then
        echo "Skipping $type, config file already exists: $file"
        return 0
    fi

    . "$generator"

    echo "Generating $1 file: $2"

    $fn "$file"

    echo "Please review the generated file at: $file"
    if [ -n "$todos" ]
    then
        echo "In particular, review & fix any item marked #TODO"
        echo
    fi
}
