#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

KLUS_SETUP_FORCE=""
KLUS_SETUP_OVERWRITE=""
KLUS_SETUP_WIPE_DIRECTORIES=""
KLUS_SETUP_CUSTOM=""
KLUS_SETUP_MISSING_PROGS=""
KLUS_SETUP_RC_KEEP_SETTINGS=""

scan_for_setup_klus_opts ()
{
    while [ $# -ne 0 ]
    do
        case "$1" in
        -f|--force)
            if [ -z "$KLUS_SETUP_FORCE" ]
            then
                KLUS_SETUP_FORCE="true"
            else
                echo "Duplicate option: $1" >&2
                return 1
            fi
        ;;
        -k|--keep)
            if [ -z "$KLUS_SETUP_RC_KEEP_SETTINGS" ]
            then
                KLUS_SETUP_RC_KEEP_SETTINGS="true"
            else
                echo "Duplicate option: $1" >&2
                return 1
            fi
        ;;
        -o|--overwrite)
            if [ -z "$KLUS_SETUP_OVERWRITE" ]
            then
                KLUS_SETUP_OVERWRITE="true"
            else
                echo "Duplicate option: $1" >&2
                return 1
            fi
        ;;
        -w|--wipe)
            if [ -z "$KLUS_SETUP_WIPE_DIRECTORIES" ]
            then
                KLUS_SETUP_WIPE_DIRECTORIES="true"
            else
                echo "Duplicate option: $1" >&2
                return 1
            fi
        ;;
        -c|--custom)
            if [ -z "$KLUS_SETUP_CUSTOM" ]
            then
                KLUS_SETUP_CUSTOM="true"
            else
                echo "Duplicate option: $1" >&2
                return 1
            fi
        ;;
        *)
            echo "Invalid option: $1" >&2
            return 1
        ;;
        esac
        shift 1
    done
}

check_force_setup_klus ()
{
    local klusrc="$1"
    local klusdir="$2"
    if [ "$klusdir" != "$(pwd)" ] && [ -z "$KLUS_SETUP_FORCE" ]
    then
        echo "A klusrc was found: $klusrc" >&2
        echo "It looks like Klus is already set up for this directory: $klusdir" >&2
        echo "Cowardly refusing to run setup without --force" >&2
        setup_klus_help >&2
        exit 201
    fi

    setup_klus_configs
}

setup_klus_config_is_external ()
{
    local cfg="$(readlink -m "$1")"
    if [ "$ROOT" = "$(dirname "$cfg")" ]
    then
        return 1
    else
        echo "Not touching config file because it is 'external' to $ROOT ... $1"
        return 0
    fi
}

setup_klus_rcfile ()
{
    if [ -f "$KLUS_RC" ] && [ -n "$KLUS_SETUP_RC_KEEP_SETTINGS" ]
    then
        echo "Loading existing klusrc: $KLUS_RC"
        . "$KLUS_RC"
    fi

    if [ "$KDE_DIST_DIR" = "$KDE_SRC_DIR" ]
    then
        echo "Conflicting KDE_SRC_DIR and KDE_DIST_DIR settings: $KDE_SRC_DIR" >&2
        return 1
    elif [ "$KDE_SRC_DIR" = "$KDESRC_BUILD_DIR" ]
    then
        echo "Conflicting KDESRC_BUILD_DIR and KDE_SRC_DIR settings: $KDE_SRC_DIR" >&2
        return 1
    elif [ "$KDE_DIST_DIR" = "$KDESRC_BUILD_DIR" ]
    then
        echo "Conflicting KDESRC_BUILD_DIR and KDE_DIST_DIR settings: $KDE_DIST_DIR" >&2
        return 1
    fi

    klus_generate_config klusrc "$KLUS_RC" \
        "$KLUS_SCRIPT_DIR/util/config-generators.sh" generate_klusrc \
        "$KLUS_SETUP_OVERWRITE" "true"
}

setup_klus_config_file ()
{
    local type="$1"
    local file="$2"
    local generator="$3"
    local todos="$4"

    local cfg="$(readlink -m "$file")"
    if [ "$ROOT" = "$(dirname "$cfg")" ]
    then
        klus_generate_config "$type" "$cfg" "$KLUS_SCRIPT_DIR/util/config-generators.sh" "$generator" "$KLUS_SETUP_OVERWRITE" "$todos" \
            || setup_klus_fail "Failed to set up $type: $cfg"
    else
        echo "Not touching $type file because it is 'external' to $ROOT ... $cfg"
    fi
}

setup_klus_kdesrc_build_clone ()
{
    local dir="$(readlink -m "$KDESRC_BUILD_DIR")"

    if [ "$ROOT" = "$(dirname "$dir")" ]
    then
        if [ -d "$dir" -a -z "$KLUS_SETUP_WIPE_DIRECTORIES" ]
        then
            echo "Skipping git clone of kdesrc-build, because the directory already exists: $dir"
        else
            echo "Cloning kdesrc-build to: $dir"
            rm -rf "$dir" && git clone https://anongit.kde.org/kdesrc-build "$dir" || setup_klus_fail "Failed to clone kdesrc-build"
        fi
    else
        echo "Not touching kdesrc-build clone because it is 'external' to $ROOT ... $dir"
    fi
}

setup_klus_kde_identity ()
{
    local identity="$(readlink -m "$KDE_IDENTITY")"

    if [ "$ROOT" = "$(dirname "$identity")" ]
    then
        if [ -f "$identity" ]
        then
            echo "Skipping KDE identity SSH key pair, because the file already exists: $identity"
        else
            echo "Generating a KDE identity SSH key pair: $identity"
            echo
            ssh-keygen -t ed25519 -f "$identity"
            echo
            echo "Please configure your KDE (git) accounts with this SSH public key: $identity.pub"
        fi
    else
        echo "Not touching KDE identity SSH key pair because it is 'external' to $ROOT ... $identity"
    fi
}

setup_klus_fail ()
{
    echo "$1" >&2
    exit 1
}

setup_klus_managed_directory ()
{
    local type="$1"
    local dir="$(readlink -m "$2")"

    if [ -d "$dir" -a -z "$KLUS_SETUP_WIPE_DIRECTORIES" ]
    then
        echo "Skipping $type directory, because it already exists: $dir"
    else
        rm -rf "$dir" && mkdir -p "$dir" || setup_klus_fail "Failed to (re)create $type directory: $dir"
    fi
}

setup_klus_configs ()
{
    . "$KLUS_SCRIPT_DIR/util/generate-config-file.sh"

    setup_klus_rcfile || setup_klus_fail "Failed to set up klusrc: $KLUS_RC"
    if [ -n "$KLUS_SETUP_CUSTOM" ]
    then
        echo
        echo "Stopping here so you can customise: $KLUS_RC"
        return 0
    fi

    KLUS_GIT_USER_WARNING=""
    KLUS_GIT_USER="$(git config user.name)" || \
        KLUS_GIT_USER="$(git config --global user.name)" || \
        KLUS_GIT_USER_WARNING="true"

    if [ -n "$KLUS_GIT_USER_WARNING" ]
    then
        KLUS_GIT_USER="#TODO: replace this with your name"
    fi

    KLUS_GIT_EMAIL_WARNING=""
    KLUS_GIT_EMAIL="$(git config user.email)" || \
        KLUS_GIT_EMAIL="$(git config --global user.email)" || \
        KLUS_GIT_EMAIL_WARNING="true"

    if [ -n "$KLUS_GIT_EMAIL_WARNING" ]
    then
        KLUS_GIT_EMAIL="#TODO: replace this with your e-mail address"
    fi

    setup_klus_managed_directory source "$KDE_SRC_DIR"
    setup_klus_managed_directory installation "$KDE_DIST_DIR"
    setup_klus_config_file kdesrc-buildrc "$KDESRC_BUILD_RC" \
        generate_kdesrc_build_rc "${KLUS_GIT_USER_WARNING}${KLUS_GIT_EMAIL_WARNING}"
    setup_klus_config_file .gitconfig "$GIT_CONFIG" \
        generate_gitconfig "${KLUS_GIT_USER_WARNING}${KLUS_GIT_EMAIL_WARNING}"
    setup_klus_config_file .ssh/config "$SSH_CONFIG" \
        generate_ssh_config
    setup_klus_config_file .ssh/known_hosts "$SSH_HOSTS" \
        generate_ssh_hosts
    setup_klus_config_file arc.rc "$ARC_RC" \
        generate_arc_rc
    setup_klus_kde_identity
    setup_klus_kdesrc_build_clone
}

try_setup_klus ()
{
    ROOT="$(pwd)"
    KLUS_RC="$ROOT/klusrc"
    KDESRC_BUILD_DIR="$ROOT/kdesrc-build"
    KDE_SRC_DIR="$ROOT/src"
    KDE_DIST_DIR="$ROOT/dist"
    KDESRC_BUILD_RC="$ROOT/kdesrc-build.rc"
    ARC_RC="$ROOT/arc.rc"
    GIT_CONFIG="$ROOT/git.conf"
    SSH_CONFIG="$ROOT/ssh.conf"
    SSH_HOSTS="$ROOT/ssh_hosts.conf"
    KDE_IDENTITY="$ROOT/kde-identity"
    CONTAINER_MEMORY_LIMIT="24g"

    . "$KLUS_SCRIPT_DIR/util/find-program.sh"

    if [ -z "$KLUS_SETUP_CUSTOM" ] && klus_programs_are_missing git
    then
        echo "Missing required programs: setup won't be able to continue" >&2
        exit 201
    fi

    . "$KLUS_SCRIPT_DIR/util/find-klusrc.sh"
    find_klus_rc check_force_setup_klus setup_klus_configs
}

setup_klus_help ()
{
    cat << KLUS_SETUP_HELP
$KLUS_USAGE_SCRIPT_PREAMBLE : setup configuration files for Klus commands

Setup always runs in the working directory, which is currently: $(pwd)

Getting help:

 $KLUS_USAGE_SCRIPT_PREAMBLE -h|--help

Running setup:

 $KLUS_USAGE_SCRIPT_PREAMBLE [options]

Options:

 -c, --custom    : stop after generating klusrc file, allowing you to customise it before re-running setup
 -f, --force     : force setup even if Klus is already set up in a parent directory
 -k, --keep      : keep existing klusrc settings
 -o, --overwrite : allow overwriting or updating existing configuration files
 -w, --wipe      : wipe (rm -rf) existing source and installation directories managed by Klus

KLUS_SETUP_HELP
}

klus ()
{
    case "$1" in
        -h|--help)
            setup_klus_help
            exit 0
        ;;
        *)
            if scan_for_setup_klus_opts "$@"
            then
                try_setup_klus
            else
                setup_klus_help >&2
                exit 200
            fi
        ;;
    esac
}
