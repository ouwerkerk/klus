#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

klus_get_kdesrc_build_mounts ()
{
    echo -n " -v \"$KDESRC_BUILD_DIR:/kdesrc-build/bin:ro\""
    echo -n " -v \"$KDESRC_BUILD_RC:$HOME/.kdesrc-buildrc:ro\""
    echo -n " -v \"$GIT_CONFIG:$HOME/.gitconfig:ro\""
}

klus ()
{
    . "$KLUS_SCRIPT_DIR/util/container-tool.sh"
    container_tool_klus kdesrc-build klus_get_kdesrc_build_mounts "$@"
}
