#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

KLUS_FLATPAK_ARGS=""
KLUS_FLATPAK_WIPE=""
KLUS_FLATPAK_SETUP=""
KLUS_FLATPAK_BUILD_DIR=""
KLUS_FLATPAK_MANIFEST=""
KLUS_FLATPAK_BUILDER_HELP=""
KLUS_FLATPAK_AUTOREMOVE_DIR="true"
KLUS_FLATPAK_KEEP_DIR=""

save_arg_for_flatpak_klus ()
{
    if [ -n "$KLUS_FLATPAK_ARGS" ]
    then
        KLUS_FLATPAK_ARGS="$KLUS_FLATPAK_ARGS $1"
    else
        KLUS_FLATPAK_ARGS="$1"
    fi

    case "$1" in
        -h|--help)
            KLUS_FLATPAK_BUILDER_HELP="true"
        ;;
        *)
            if [ -n "$KLUS_FLATPAK_BUILD_DIR" -a -f "$1" -a -z "$KLUS_FLATPAK_MANIFEST" ]
            then
                KLUS_FLATPAK_MANIFEST="$1"
            elif [ -d "$1" -o ! -e "$1" ]
            then
                KLUS_FLATPAK_MANIFEST=""
                KLUS_FLATPAK_BUILD_DIR="$1"
            else
                KLUS_FLATPAK_MANIFEST=""
                KLUS_FLATPAK_BUILD_DIR=""
            fi
        ;;
    esac
}

scan_for_flatpak_klus_opts ()
{
    KLUS_OPTS_STOP=""
    while [ $# -ne 0 ]
    do
        if [ -z "$KLUS_OPTS_STOP" ]
        then
            case "$1" in
            --)
                KLUS_OPTS_STOP="true"
            ;;
            --force-clean)
                KLUS_FLATPAK_AUTOREMOVE_DIR=""
                save_arg_for_flatpak_klus "$1"
            ;;
            --build-shell*)
                KLUS_FLATPAK_AUTOREMOVE_DIR=""
                save_arg_for_flatpak_klus "$1"
            ;;
            -p|--preserve)
                if [ -z "$KLUS_FLATPAK_KEEP_DIR" ]
                then
                    KLUS_FLATPAK_KEEP_DIR="true"
                    KLUS_FLATPAK_AUTOREMOVE_DIR=""
                else
                    echo "Duplicate/conflicting option: $1" >&2
                    return 1
                fi
            ;;
            -a|--again)
                if [ -z "$KLUS_FLATPAK_SETUP" -a -z "$KLUS_FLATPAK_WIPE" ]
                then
                    KLUS_FLATPAK_WIPE="true"
                    KLUS_FLATPAK_SETUP="true"
                else
                    echo "Duplicate/conflicting option: $1" >&2
                    return 1
                fi
            ;;
            -s|--setup)
                if [ -z "$KLUS_FLATPAK_SETUP" ]
                then
                    KLUS_FLATPAK_SETUP="true"
                else
                    echo "Duplicate/conflicting option: $1" >&2
                    return 1
                fi
            ;;
            -w|--wipe)
                if [ -z "$KLUS_FLATPAK_WIPE" ]
                then
                    KLUS_FLATPAK_WIPE="true"
                else
                    echo "Duplicate/conflicting option: $1" >&2
                    return 1
                fi
            ;;
            *)
                save_arg_for_flatpak_klus "$1"
            ;;
            esac
        else
            save_arg_for_flatpak_klus "$1"
        fi
        shift 1
    done
}

klus_flatpak_wipe ()
{
    echo "About to destructively purge your flatpak user installation"
    read -p "Last chance to abort ... (^C to abort) " -r FOO || true

    # this should generally do the trick
    rm -rf "$HOME/.local/share/flatpak"
}

klus_flatpak_repo_data ()
{
    cat << REPOSITORIES
flathub https://flathub.org/repo/flathub.flatpakrepo
kdeapps https://distribute.kde.org/kdeapps.flatpakrepo
REPOSITORIES
}

klus_flatpak_setup_repo ()
{
    echo "Setting up repo: $@"
    flatpak remote-add --if-not-exists --user "$@"
}

klus_flatpak_setup_repos ()
{
    klus_flatpak_repo_data | while read -r repo || [ -n "$repo" ]
    do
        if [ -n "$repo" ]
        then
            klus_flatpak_setup_repo $repo # do not quote: contains 2 args: alias & url
        fi
    done
}

klus_flatpak_setup ()
{
    if klus_flatpak_setup_repos
    then
        return 0
    else
        cat 1>&2 << REPO_ERROR
Unable to set up repositories, not installing the flatpak Builder.
The following repositories are needed:

$(klus_flatpak_repo_data)

You can try adding them manually. For each pair of <alias> and <url> listed,
try running:

  flatpak remote-add --user <alias> <url>

REPO_ERROR
        return 1
    fi
}

klus_flatpak_fail ()
{
    echo "$1" >&2
    echo "Cowardly refusing to continue" >&2
    exit 202
}

try_flatpak_klus ()
{
    if [ -n "$KLUS_FLATPAK_WIPE" ]
    then
        klus_flatpak_wipe || klus_flatpak_fail "Failed to wipe your flatpak user installation"
    fi

    . "$KLUS_SCRIPT_DIR/util/find-program.sh"


    if [ -n "$KLUS_FLATPAK_SETUP" ]
    then
        if klus_programs_are_missing flatpak
        then
            echo "Missing required programs: cowardly refusing to run flatpak" >&2
            exit 201
        fi
        klus_flatpak_setup || klus_flatpak_fail "Failed to setup your flatpak user installation"
    fi

    if klus_programs_are_missing flatpak-builder eu-strip
    then
        echo "Missing required programs: cowardly refusing to run flatpak-builder" >&2
        exit 201
    fi

    if [ -n "$KLUS_FLATPAK_BUILDER_HELP" ]
    then
        flatpak-builder --help
    elif [ -n "$KLUS_FLATPAK_ARGS" ]
    then
        if [ -z "$KLUS_FLATPAK_KEEP_DIR" -a -n "$KLUS_FLATPAK_AUTOREMOVE_DIR" ]
        then
            rm -rf "$KLUS_FLATPAK_BUILD_DIR"
        else
            echo "Preserving existing flatpak build directory if any: $KLUS_FLATPAK_BUILD_DIR"
        fi
        set -x
        flatpak-builder --user --install --rebuild-on-sdk-change --install-deps-from=flathub $KLUS_FLATPAK_ARGS
    fi
}

flatpak_klus_help ()
{
    cat << FLATPAK_KLUS_HELP
$KLUS_USAGE_SCRIPT_PREAMBLE : helps you put flatpak applications together
This is a convenience wrapper script around flatpak-builder.

Getting help:

   $KLUS_USAGE_SCRIPT_PREAMBLE -h|--help
   $KLUS_USAGE_SCRIPT_PREAMBLE -- --help # for flatpak-builder help

Running flatpak-builder:

  $KLUS_USAGE_SCRIPT_PREAMBLE [options] <build-directory> <flatpak-manifest.json>

Use -- to stop processing options and pass everything following -- as arguments to flatpak-builder:

  $KLUS_USAGE_SCRIPT_PREAMBLE [options] -- [builder args]

Options:

 -p, --preserve : do not wipe old build directories, use when passing --build-shell to flatpak-builder
 -a, --again    : (forcibly) wipe an existing flatpak user installation and re-run setup
 -s, --setup    : configure necessary flatpak repositories
 -w, --wipe     : purge (rm -rf) your local flatpak user installation

FLATPAK_KLUS_HELP
}

klus ()
{
    case "$1" in
        -h|--help)
            flatpak_klus_help
            exit 0
        ;;
        *)
            if scan_for_flatpak_klus_opts "$@"
            then
                if [ -z "$KLUS_FLATPAK_BUILDER_HELP" ] && [ -z "$KLUS_FLATPAK_MANIFEST" -o -z "$KLUS_FLATPAK_BUILD_DIR" ]
                then
                    echo "Missing a flatpak manifest or build directory" >&2
                    echo "It looks like these were not specified on the commandline" >&2
                    echo "Cowardly refusing to run flatpak-builder" >&2
                    exit 200
                fi

                try_flatpak_klus
            else
                flatpak_klus_help >&2
                exit 200
            fi
        ;;
    esac
}
