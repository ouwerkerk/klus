#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

KLUS_OPTS_STOP=""
KLUS_APK_SHELL=""
KLUS_APK_ARGS=""
KLUS_APK_APP=""
KLUS_APK_APP_NEEDED="true"
KLUS_APK_DIST_DIR=""
KLUS_APK_SRC_DIR=""
KLUS_APK_SHELL=""
KLUS_APK_WIPE=""
kLUS_APK_WIPE_TOOLING=""
KLUS_APK_ARCH_ARM32="true"
KLUS_APK_ARCH_ARM64="true"

apk_looks_like_app ()
{
    case "$1" in
        sh|bash)
            return 1
        ;;
        /opt/helpers/*|build-*)
            return 1
        ;;
        *)
            return 0
        ;;
    esac
}

save_arg_for_apk_klus ()
{
    if [ -z "$KLUS_APK_APP" ] && apk_looks_like_app "$1"
    then
        KLUS_APK_APP="$1"
    elif [ -n "$KLUS_APK_ARGS" ]
    then
        KLUS_APK_ARGS="$KLUS_APK_ARGS $1"
    else
        KLUS_APK_ARGS="$1"
    fi
}

set_apk_klus_arch ()
{
    case "$1" in
    arm|arm32|arm-v7a|armv7a)
        KLUS_APK_ARCH_ARM64=""
        KLUS_APK_ARCH_ENVVARS="-e ONLY_ARM32=1"
    ;;
    arm64|arm64-v8a|aarch64)
        KLUS_APK_ARCH_ARM32=""
        KLUS_APK_ARCH_ENVVARS="-e ONLY_ARM64=1"
    ;;
    x86|i686)
        echo "The KDE Android SDK does not support this Android architecture yet: $1" >&2
        return 1
    ;;
    x64|x86-64|x86_64)
        echo "The KDE Android SDK does not support this Android architecture yet: $1" >&2
        return 1
    ;;
    *)
        echo "Unknown/invalid Android architecture: $1" >&2
        return 1
    ;;
    esac
}

scan_for_apk_klus_opts ()
{
    KLUS_OPTS_STOP=""
    while [ $# -ne 0 ]
    do
        if [ -z "$KLUS_OPTS_STOP" ]
        then
            case "$1" in
            --)
                KLUS_OPTS_STOP="true"
            ;;
            -a|--arch)
                if [ -n "$KLUS_APK_ARCH_ENVVARS" ]
                then
                    echo "Duplicate/conflicting option: $1" >&2
                    return 1
                elif [ -z "$2" ]
                then
                    echo "An Android architecture is required for option: $1" >&2
                    return 1
                elif set_apk_klus_arch "$2"
                then
                    shift 1
                else
                    return 1
                fi
            ;;
            -t|--wipe-tooling)
                if [ -n "$KLUS_APK_WIPE_TOOLING" ]
                then
                    echo "Duplicate option: $1" >&2
                    return 1
                else
                    kLUS_APK_WIPE_TOOLING="true"
                fi
            ;;
            -w|--wipe)
                if [ -n "$KLUS_APK_WIPE" ]
                then
                    echo "Duplicate option: $1" >&2
                    return 1
                else
                    KLUS_APK_WIPE="true"
                fi
            ;;
            -z|--shell)
                if [ - n "$KLUS_APK_SHELL" ]
                then
                    echo "Duplicate option: $1" >&2
                    return 1
                else
                    KLUS_APK_SHELL="bash"
                    KLUS_APK_APP_NEEDED=""
                fi
            ;;
            -s|--source)
                if [ -n "$KLUS_APK_SRC_DIR" ]
                then
                    echo "Duplicate option: $1" >&2
                    return 1
                elif [ -z "$2" ]
                then
                    echo "A directory is required for option: $1" >&2
                    return 1
                elif [ ! -d "$2" ]
                then
                    echo "Not a valid directory: $2" >&2
                    return 1
                elif [ "$(readlink -m $2)" = "$KLUS_APK_DIST_DIR" ]
                then
                    echo "Already used as output directory: $2" >&2
                    return 1
                else
                    KLUS_APK_SRC_DIR="$(readlink -m $2)"
                    shift 1
                fi
            ;;
            -o|--output)
                if [ -n "$KLUS_APK_DIST_DIR" ]
                then
                    echo "Duplicate option: $1" >&2
                    return 1
                elif [ -z "$2" ]
                then
                    echo "A directory is required for option: $1" >&2
                    return 1
                elif [ -e "$2" -a ! -d "$2" ]
                then
                    echo "Not a valid directory: $2" >&2
                    return 1
                elif [ "$(readlink -m $2)" = "$KLUS_APK_SRC_DIR" ]
                then
                    echo "Already used as source directory: $2" >&2
                    return 1
                else
                    KLUS_APK_DIST_DIR="$(readlink -m $2)"
                    shift 1
                fi
            ;;
            *)
                save_arg_for_apk_klus "$1"
            ;;
            esac
        else
            save_arg_for_apk_klus "$1"
        fi

        shift 1
    done
}

get_sdk_klus_command ()
{
    local memlimit=""
    if [ -n "$CONTAINER_MEMORY_LIMIT" ]
    then
        memlimit="--memory-swap $CONTAINER_MEMORY_LIMIT --memory $CONTAINER_MEMORY_LIMIT"
    fi

    local buildflags=""
    if [ -n "$DEFAULT_JOB_COUNT" ]
    then
        buildflags="-e MAKEFLAGS=\"-j $DEFAULT_JOB_COUNT\" -e NINJAFLAGS=\"-j $DEFAULT_JOB_COUNT\""
    fi

    local src=""
    if [ -n "$KLUS_APK_SRC_DIR" ]
    then
        src="-v \"$KLUS_APK_SRC_DIR:/home/user/src/$KLUS_APK_APP\""
    fi

    local tooling="/tmp/kde-android-tooling-home"
    local dist=""
    if [ -n "$KLUS_APK_DIST_DIR" ]
    then
        tooling="$KLUS_APK_DIST_DIR/kde-android-tooling-home"
        dist="-v \"$KLUS_APK_DIST_DIR/output:/output\""
        if [ -n "$KLUS_APK_ARCH_ARM32" ]
        then
            dist="$dist -v \"$KLUS_APK_DIST_DIR/arm:/home/user/build-arm\""
        fi
        if [ -n "$KLUS_APK_ARCH_ARM64" ]
        then
            dist="$dist -v \"$KLUS_APK_DIST_DIR/arm64:/home/user/build-arm64\""
        fi
    fi

    local entrypoint="/opt/helpers/build-generic $KLUS_APK_APP"
    if [ -n "$KLUS_APK_SHELL" ]
    then
        entrypoint="bash"
    fi

    #
    # Run the tool inside the container
    #
    #  - as'root' so the container will be able to write to mounted host volumes (if any)
    #    on the host this actually maps to the user that runs the script
    #  - mapping a fixed source directory
    #  - mapping a fixed destination/dist directory to 'make install' to
    #  - set up environment variables for Qt tools
    #  - pass arguments to this script on: this lets the user override CMD defaults from the container image directly.
    #    i.e. it lets the user pass options to kdesrc-build running inside the container.
    #
    echo -n podman run -it --rm \
        $memlimit \
        -u "0:0" \
        -v "\"$tooling:$HOME\"" $src $dist \
        $KLUS_APK_ARCH_ENVVARS \
        $buildflags \
        docker.io/kdeorg/android-sdk \
        $entrypoint $KLUS_APK_ARGS
}

setup_apk_klus_dir ()
{
    local dir="$1"
    local wipe="$2"
    local apply="$3"
    if [ -n "$apply" ]
    then
        if [ -n "$wipe" ]
        then
            rm -rf "$dir"
        fi
        mkdir -p "$dir"
    fi
}

run_sdk_klus ()
{
    . "$KLUS_SCRIPT_DIR/util/find-program.sh"

    if klus_programs_are_missing podman
    then
        echo "Missing required programs: cowardly refusing to run the KDE Android SDK" >&2
        exit 201
    fi

    #
    # Make sure to 'regenerate' these directories if deleted/not existing yet
    #
    if [ -n "$KLUS_APK_SRC_DIR" ]
    then
        mkdir -p "$KLUS_APK_SRC_DIR"
    fi
    if [ -n "$KLUS_APK_DIST_DIR" ]
    then
        setup_apk_klus_dir "$KLUS_APK_DIST_DIR/kde-android-tooling-home" "$KLUS_APK_WIPE_TOOLING" "true"
        setup_apk_klus_dir "$KLUS_APK_DIST_DIR/output" "$KLUS_APK_WIPE" "true"
        setup_apk_klus_dir "$KLUS_APK_DIST_DIR/arm" "$KLUS_APK_WIPE" "$KLUS_APK_ARCH_ARM32"
        setup_apk_klus_dir "$KLUS_APK_DIST_DIR/arm64" "$KLUS_APK_WIPE" "$KLUS_APK_ARCH_ARM64"
    else
        mkdir -p /tmp/kde-android-tooling-home
    fi

    local sdk_command="$(get_sdk_klus_command)"

    set -x
    eval $sdk_command
}

apply_rc_to_apk_klus ()
{
    local klusrc="$1"

    echo "Loading existing klusrc: $klusrc"
    . "$klusrc"

    run_sdk_klus
}

try_apk_klus ()
{
    . "$KLUS_SCRIPT_DIR/util/find-klusrc.sh"
    find_klus_rc apply_rc_to_apk_klus run_sdk_klus
}

apk_klus_help ()
{
    cat << APK_KLUS_HELP
$KLUS_USAGE_SCRIPT_PREAMBLE : build an Android app using the KDE Android SDK

This is a convenience wrapper around the KDE Android SDK container image.
See also: https://community.kde.org/Android/Environment_via_Container

Getting help:

 $KLUS_USAGE_SCRIPT_PREAMBLE -h|--help

Entering the KDE Android SDK container:

 $KLUS_USAGE_SCRIPT_PREAMBLE [options] <app> [options|build args]

Use -- to stop processing options and pass everything following -- as arguments to the container:

 $KLUS_USAGE_SCRIPT_PREAMBLE [options] <app> [build args] -- [build args]

Options:

 -a, --arch <arch>  : build only for a given Android architecture supported by the KDE Android SDK
 -s, --source <dir> : source directory to mount (instead of cloning the KDE app in the container)
 -o, --output <dir> : output directory to mount
 -t, --wipe-tooling : wipe cached Gradle and other Android SDK tooling, only applies if --output is used
 -w, --wipe         : wipe previous build ouput (if any), only applies if --output is used
 -z, --shell        : enter a (bash) shell instead of building

APK_KLUS_HELP
}

klus ()
{
    case "$1" in
        -h|--help)
            apk_klus_help
            exit 0
        ;;
        *)
            if scan_for_apk_klus_opts "$@"
            then
                if [ -n "$KLUS_APK_APP_NEEDED" -a -z "$KLUS_APK_APP" ]
                then
                    echo "No app specified?" >&2
                    echo "It looks like no app to build was specified" >&2
                    echo "Cowardly refusing to run the KDE Android SDK" >&2
                    exit 200
                fi

                try_apk_klus
            else
                apk_klus_help >&2
                exit 200
            fi
        ;;
    esac
}
