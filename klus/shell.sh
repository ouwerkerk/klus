#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

KLUS_SHELL_ARGS=""
KLUS_SHELL="$SHELL"

shell_klus_help ()
{
    cat << SHELL_KLUS_HELP
$KLUS_USAGE_SCRIPT_PREAMBLE : enter a shell with predefined KDE environment variables

Getting help:

 $KLUS_USAGE_SCRIPT_PREAMBLE -h|--help

Running the shell ($KLUS_SHELL)

 $KLUS_USAGE_SCRIPT_PREAMBLE [... shell args]
SHELL_KLUS_HELP
}

klus_shell_prepend_path ()
{
    local head="$1"
    local tail="$2"

    if [ ! -d "$head" ]
    then
        echo -n "$tail"
    elif [ -n "$tail" ]
    then
        echo -n "${head}:${tail}"
    else
        echo -n "$head"
    fi
}

klus_shell_auto_prepend_lib_paths ()
{
    local prefix="$1"
    local suffix="$2"
    local result="$3"

    result="$(klus_shell_prepend_path "$prefix/$suffix" "$result")"
    result="$(klus_shell_prepend_path "$prefix/lib/$suffix" "$result")"
    result="$(klus_shell_prepend_path "$prefix/lib64/$suffix" "$result")"
    result="$(klus_shell_prepend_path "$prefix/lib/x86_64-linux-gnu/$suffix" "$result")"
    echo -n "$result"
}

klus_config_found ()
{
    local klusrc="$1"
    local klusdir="$2"

    echo "Loading existing klusrc: $klusrc"
    . "$klusrc"

    PATH="$(klus_shell_prepend_path "$KDE_DIST_DIR/bin" "$PATH")"
    CMAKE_PREFIX_PATH="$(klus_shell_prepend_path "$KDE_DIST_DIR" "$CMAKE_PREFIX_PATH")"
    QML2_IMPORT_PATH="$(klus_shell_auto_prepend_lib_paths "$KDE_DIST_DIR" "qml" "$QML2_IMPORT_PATH")"
    QT_QUICK_CONTROLS_STYLE_PATH="$(klus_shell_auto_prepend_lib_paths "$KDE_DIST_DIR" "qml/QtQuick/Controls.2" "$QT_QUICK_CONTROLS_STYLE_PATH")"
    if [ -z "$QT_QUICK_CONTROLS_STYLE" ]
    then
        QT_QUICK_CONTROLS_STYLE="Breeze"
    fi
    QT_PLUGIN_PATH="$(klus_shell_auto_prepend_lib_paths "$KDE_DIST_DIR" "qt5/plugins" "$QT_PLUGIN_PATH")"
    QT_PLUGIN_PATH="$(klus_shell_auto_prepend_lib_paths "$KDE_DIST_DIR" "plugins" "$QT_PLUGIN_PATH")"
    PKG_CONFIG_PATH="$(klus_shell_auto_prepend_lib_paths "$KDE_DIST_DIR" "pkgconfig" "$PKG_CONFIG_PATH")"
    PYTHONPATH="$(klus_shell_auto_prepend_lib_paths "$KDE_DIST_DIR" "site-packages" "$PYTHONPATH")"
    LD_LIBRARY_PATH="$(klus_shell_auto_prepend_lib_paths "$KDE_DIST_DIR" "" "$LD_LIBRARY_PATH")"
    MANPATH="$(klus_shell_prepend_path "$KDE_DIST_DIR/share/man" "$MANPATH")"
    XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"
    XDG_DATA_DIRS="$(klus_shell_prepend_path "$KDE_DIST_DIR/share" "$XDG_DATA_DIRS")"
    XDG_CONFIG_DIRS="$(klus_shell_prepend_path "$KDE_DIST_DIR/etc/xdg" "$XDG_CONFIG_DIRS")"

    set -x
    export PATH="$PATH" MANPATH="$MANPATH" PYTHONPATH="$PYTHONPATH" LD_LIBRARY_PATH="$LD_LIBRARY_PATH" \
        QT_PLUGIN_PATH="$QT_PLUGIN_PATH" QML2_IMPORT_PATH="$QML2_IMPORT_PATH" \
        QT_QUICK_CONTROLS_STYLE_PATH="$QT_QUICK_CONTROLS_STYLE_PATH" QT_QUICK_CONTROLS_STYLE="$QT_QUICK_CONTROLS_STYLE" \
        XDG_DATA_DIRS="$XDG_DATA_DIRS" XDG_CONFIG_DIRS="$XDG_CONFIG_DIRS" XDG_CONFIG_HOME="$XDG_CONFIG_HOME" \
        CMAKE_PREFIX_PATH="$CMAKE_PREFIX_PATH" PKG_CONFIG_PATH="$PKG_CONFIG_PATH"
    $KLUS_SHELL $KLUS_SHELL_ARGS
}

klus_config_not_found ()
{
    local klusdir="$1"
    if [ -z "$KLUS_TEST_QUIET" -a -z "$KLUS_TEST_PRINT_ROOT_DIR" ]
    then
        echo "No klusrc was detected"
        echo "It looks like Klus is not yet set up for: $KLUS_TEST_DIR"
    fi
    exit 1
}

try_klus_shell ()
{
    . "$KLUS_SCRIPT_DIR/util/find-klusrc.sh"

    if [ -z "$KLUS_TEST_DIR" ]
    then
        KLUS_TEST_DIR="$(pwd)"
    fi

    if [ -d "$KLUS_TEST_DIR" ]
    then
        cd "$KLUS_TEST_DIR"
    fi
    find_klus_rc klus_config_found klus_config_not_found
}

klus ()
{
    if [ -z "$KLUS_SHELL" ]
    then
        KLUS_SHELL="/bin/sh"
    fi

    case "$1" in
        -h|--help)
            shell_klus_help
            exit 0
        ;;
        *)
            KLUS_SHELL_ARGS="$@"
            try_klus_shell
        ;;
    esac
}
