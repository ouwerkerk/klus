#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

KLUS_CLEANUP_ALL=""
KLUS_CLEANUP_RC_FILE=""
KLUS_CLEANUP_FORCE=""
KLUS_CLEANUP_SOURCE=""
KLUS_CLEANUP_KDESRC_BUILD=""
KLUS_CLEANUP_DIST="true"
KLUS_CLEANUP_IDENTITY=""
KLUS_CLEANUP_CONFIGS=""

scan_for_cleanup_klus_opts ()
{
    while [ $# -ne 0 ]
    do
        case "$1" in
        -a|--all)
            if [ -z "$KLUS_CLEANUP_ALL" ]
            then
                KLUS_CLEANUP_ALL="true"
                KLUS_CLEANUP_RC_FILE="true"
                KLUS_CLEANUP_CONFIGS="true"
                KLUS_CLEANUP_SOURCE="true"
                KLUS_CLEANUP_DIST="true"
                KLUS_CLEANUP_IDENTITY="true"
                KLUS_CLEANUP_KDESRC_BUILD="true"
            else
                echo "Duplicate option: $1" >&2
                return 1
            fi
        ;;
        -f|--force)
            if [ -z "$KLUS_CLEANUP_FORCE" ]
            then
                KLUS_CLEANUP_FORCE="true"
            else
                echo "Duplicate option: $1" >&2
                return 1
            fi
        ;;
        -c|--config)
            if [ -z "$KLUS_CLEANUP_CONFIGS" ]
            then
                KLUS_CLEANUP_CONFIGS="true"
            else
                echo "Duplicate option: $1" >&2
                return 1
            fi
        ;;
        -i|--identity)
            if [ -z "$KLUS_CLEANUP_IDENTITY" ]
            then
                KLUS_CLEANUP_IDENTITY="true"
            else
                echo "Duplicate option: $1" >&2
                return 1
            fi
        ;;
        -s|--source)
            if [ -z "$KLUS_CLEANUP_SOURCE" ]
            then
                KLUS_CLEANUP_SOURCE="true"
            else
                echo "Duplicate option: $1" >&2
                return 1
            fi
        ;;
        -r|--rc)
            if [ -z "$KLUS_CLEANUP_RC_FILE" ]
            then
                KLUS_CLEANUP_RC_FILE="true"
            else
                echo "Duplicate option: $1" >&2
                return 1
            fi
        ;;
        -k|--kdesrc-build)
            if [ -z "$KLUS_CLEANUP_KDESRC_BUILD" ]
            then
                KLUS_CLEANUP_KDESRC_BUILD="true"
            else
                echo "Duplicate option: $1" >&2
                return 1
            fi
        ;;
        *)
            echo "Invalid option: $1" >&2
            return 1
        ;;
        esac
        shift 1
    done
}


cleanup_klus_help ()
{
    cat << CLEANUP_KLUS_HELP
$KLUS_USAGE_SCRIPT_PREAMBLE : clean up the Klus environment

Getting help:

 $KLUS_USAGE_SCRIPT_PREAMBLE -h|--help

Running clean up:

 $KLUS_USAGE_SCRIPT_PREAMBLE [options]

Options:

 -a, --all          : clean up everything managed by Klus (implies --config, --source, --kdesrc-build, --identity, --rc)
                      Warning: after clean up, the directory will no longer be recognised as a Klus environment.
 -c, --config       : clean up Klus config files as well
 -f, --force        : force removal of Klus files even if they are external to the Klus directory being cleaned
                      Warning: danger! Check your klusrc to ensure you do not lose files which you rely on outside of the Klus environment.
 -s, --source       : clean up KDE sources as well
 -k, --kdesrc-build : clean up kdesrc-build as well
 -i, --identity     : clean up KDE identity SSH key pair as well
 -r, --rc           : clean up Klus rc file itself as well.
                      Warning: after clean up, the directory will no longer be recognised as a Klus environment.

CLEANUP_KLUS_HELP
}

klus_cleanup_dir ()
{
    local dir="$(readlink -m "$1")"
    local test="$2"
    if [ -d "$dir" ]
    then
        if [ -n "$test" ]
        then
            echo "Removing ... $dir"
            rm -rf "$dir"
        else
            echo "Not removing directory ... $dir"
        fi
    fi
}

klus_cleanup_config ()
{
    local file="$(readlink -m "$1")"
    local root="$2"

    if [ -f "$file" ]
    then
        if [ "$root" = "$(dirname "$file")" ] || [ -n "$KLUS_CLEANUP_FORCE" ]
        then
            echo "Removing ... $file"
            rm -f "$file"
        else
            echo "Not removing file because it is 'external to $root ... $file"
        fi
    fi
}

klus_run_cleanup ()
{
    local klusrc="$1"
    local root="$2"

    echo "Loading existing klusrc: $klusrc"
    . "$klusrc"

    echo "Cleaning up Klus directory: $root"
    klus_cleanup_dir "$KDE_DIST_DIR" "$KLUS_CLEANUP_DIST"
    klus_cleanup_dir "$KDE_SRC_DIR" "$KLUS_CLEANUP_SOURCE"
    klus_cleanup_dir "$KDESRC_BUILD_DIR" "$KLUS_CLEANUP_KDESRC_BUILD"

    if [ -n "$KLUS_CLEANUP_CONFIGS" ]
    then
        klus_cleanup_config "$GIT_CONFIG" "$root"
        klus_cleanup_config "$ARC_RC" "$root"
        klus_cleanup_config "$SSH_HOSTS" "$root"
        klus_cleanup_config "$SSH_CONFIG" "$root"
        klus_cleanup_config "$KDESRC_BUILD_RC" "$root"
    fi

    if [ -n "$KLUS_CLEANUP_IDENTITY" ]
    then
        klus_cleanup_config "$KDE_IDENTITY" "$root"
        klus_cleanup_config "$KDE_IDENTITY.pub" "$root"
    fi

    if [ -n "$KLUS_CLEANUP_RC_FILE" ]
    then
        klus_cleanup_config "$klusrc" "$root"
        echo "This directory is no longer managed by Klus: $root"
    fi

    echo "Finished cleanup of: $root"
}

klus_cleanup_setup_required ()
{
    local klusdir="$1"
    echo "No klusrc was detected" >&2
    echo "It looks like Klus is not yet set up for this directory: $klusdir" >&2
    echo "Cowardly refusing to run cleanup" >&2
    exit 200
}

try_cleanup_klus ()
{
    . "$KLUS_SCRIPT_DIR/util/find-klusrc.sh"
    find_klus_rc klus_run_cleanup klus_cleanup_setup_required
}

klus ()
{
    case "$1" in
        -h|--help)
            cleanup_klus_help
            exit 0
        ;;
        *)
            if scan_for_cleanup_klus_opts "$@"
            then
                try_cleanup_klus
            else
                cleanup_klus_help >&2
                exit 200
            fi
        ;;
    esac
}
