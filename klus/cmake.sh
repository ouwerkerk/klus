#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

KLUS_OPTS_STOP=""
KLUS_CMAKE_ARGS=""
KLUS_CMAKE_AGAIN=""
KLUS_CMAKE_INSTALL=""
KLUS_CMAKE_PREFIX=""
KLUS_CMAKE_RUN_NINJA=""
KLUS_CMAKE_SANITISERS="address;leak;undefined"
KLUS_CMAKE_NEEDS_RC=""
KLUS_CMAKE_INJECTED_DEFAULTS=""
KLUS_CMAKE_SOURCE_DIR=""
KLUS_CMAKE_BUILD_TYPE=""

save_arg_for_cmake_klus ()
{
    if [ -n "$KLUS_CMAKE_ARGS" ]
    then
        KLUS_CMAKE_ARGS="$KLUS_CMAKE_ARGS $1"
    else
        KLUS_CMAKE_ARGS="$1"
    fi

    if [ -d "$1" -a -f "$1/CMakeLists.txt" -a -r "$1/CMakeLists.txt" ]
    then
        KLUS_CMAKE_SOURCE_DIR="$1"
    fi
}

scan_for_cmake_klus_opts ()
{
    KLUS_OPTS_STOP=""
    while [ $# -ne 0 ]
    do
        if [ -z "$KLUS_OPTS_STOP" ]
        then
            case "$1" in
            --)
                KLUS_OPTS_STOP="true"
            ;;
            -r|--release)
                if [ -n "$KLUS_CMAKE_SANITISERS" ]
                then
                    KLUS_CMAKE_SANITISERS=""
                else
                    echo "Duplicate option: $1" >&2
                    return 1
                fi
            ;;
            -t|--type)
                if [ -z "$KLUS_CMAKE_BUILD_TYPE" ]
                then
                    if [ -n "$2" ]
                    then
                        case "$2" in
                        Debug|RelWithDebInfo|Release|MinSizeRel)
                            KLUS_CMAKE_BUILD_TYPE="$2"
                            shift 1
                        ;;
                        *)
                            echo "Not a valid CMake build type: '$2'" >&2
                            return 1
                        ;;
                        esac
                    else
                        echo "A CMake build type is required." >&2
                        return 1
                    fi
                else
                    echo "Duplicate/conflicting option: $1" >&2
                    return 1
                fi
            ;;
            -k|--kde)
                if [ -z "$KLUS_CMAKE_INSTALL" ]
                then
                    KLUS_CMAKE_INSTALL="true"
                    KLUS_CMAKE_NEEDS_RC="true"
                else
                    echo "Duplicate option: $1" >&2
                    return 1
                fi
            ;;
            -p|--prefix)
                if [ -z "$KLUS_CMAKE_PREFIX" ]
                then
                    KLUS_CMAKE_PREFIX="true"
                    KLUS_CMAKE_NEEDS_RC="true"
                else
                    echo "Duplicate option: $1" >&2
                    return 1
                fi
            ;;
            -n|--ninja)
                if [ -z "$KLUS_CMAKE_RUN_NINJA" ]
                then
                    KLUS_CMAKE_RUN_NINJA="true"
                else
                    echo "Duplicate option: $1" >&2
                    return 1
                fi
            ;;
            -a|--again)
                if [ -z "$KLUS_CMAKE_AGAIN" ]
                then
                    KLUS_CMAKE_AGAIN="true"
                else
                    echo "Duplicate option: $1" >&2
                    return 1
                fi
            ;;
            *)
                save_arg_for_cmake_klus "$1"
            ;;
            esac
        else
            save_arg_for_cmake_klus "$1"
        fi
        shift 1
    done
}

cmake_wipe_klus ()
{
    local cmake_cache="$(pwd)/CMakeCache.txt"

    if [ -f "$cmake_cache" -a -r "$cmake_cache" ]
    then
        read -p "Last chance to abort before wiping contents of $(pwd) ... (^C to abort)" -r FOO || true
        rm -rf $(ls -A)
    else
        echo "No CMakeCache.txt found at: ${cmake_cache}" >&2
        echo "This does not look like a CMake build directory: cowardly refusing to wipe it" >&2
        exit 201
    fi
}

cmake_klus_set_defaults_from_rc ()
{
    local klusrc="$1"

    echo "Loading existing klusrc: $klusrc"
    . "$klusrc"

    if [ -z "$KDE_DIST_DIR" ]
    then
        echo "Missing KDE_DIST_DIR in klusrc: $klusrc" >&2
        echo "If Klus is not yet set up, try running: $KLUS_SCRIPT_NAME setup" >&2
        exit 201
    fi

    if [ -n "$KLUS_CMAKE_INSTALL" ]
    then
        KLUS_CMAKE_INJECTED_DEFAULTS="$KLUS_CMAKE_INJECTED_DEFAULTS -DCMAKE_INSTALL_PREFIX=\"$KDE_DIST_DIR\""
    fi
}

cmake_klus_rc_not_found ()
{
    echo "Unable to find klusrc in: $1" >&2
    echo "If Klus is not yet set up, try running: $KLUS_SCRIPT_NAME setup" >&2
    exit 201
}

try_cmake_klus ()
{
    KLUS_CMAKE_INJECTED_DEFAULTS="-G Ninja"
    if [ -n "$KLUS_CMAKE_NEEDS_RC" ]
    then
        . "$KLUS_SCRIPT_DIR/util/find-klusrc.sh"
        find_klus_rc cmake_klus_set_defaults_from_rc cmake_klus_rc_not_found
    fi

    if [ -n "$KLUS_CMAKE_BUILD_TYPE" ]
    then
        KLUS_CMAKE_INJECTED_DEFAULTS="$KLUS_CMAKE_INJECTED_DEFAULTS -DCMAKE_BUILD_TYPE=$KLUS_CMAKE_BUILD_TYPE"
    else
        KLUS_CMAKE_INJECTED_DEFAULTS="$KLUS_CMAKE_INJECTED_DEFAULTS -DCMAKE_BUILD_TYPE=RelWithDebInfo"
    fi

    if [ -n "$KLUS_CMAKE_SANITISERS" ]
    then
        KLUS_CMAKE_INJECTED_DEFAULTS="$KLUS_CMAKE_INJECTED_DEFAULTS -DECM_ENABLE_SANITIZERS=\"$KLUS_CMAKE_SANITISERS\""
    fi

    if [ -n "$KLUS_CMAKE_ARGS" ]
    then
        KLUS_CMAKE_ARGS="$KLUS_CMAKE_INJECTED_DEFAULTS $KLUS_CMAKE_ARGS"
    else
        KLUS_CMAKE_ARGS="$KLUS_CMAKE_INJECTED_DEFAULTS"
    fi

    . "$KLUS_SCRIPT_DIR/util/find-program.sh"

    if klus_programs_are_missing cmake ninja
    then
        echo "Missing required programs: cowardly refusing to run cmake" >&2
        exit 201
    fi

    if [ -n "$KLUS_CMAKE_AGAIN" ]
    then
        cmake_wipe_klus
    fi

    if [ -n "$KLUS_CMAKE_INSTALL" ]
    then
        mkdir -p "$KDE_DIST_DIR"
    fi

    if [ -n "$KLUS_CMAKE_PREFIX" ]
    then
        (
            set -x
            CMAKE_PREFIX_PATH="$KDE_DIST_DIR" cmake $KLUS_CMAKE_ARGS
        )
    else
        (
            set -x
            cmake $KLUS_CMAKE_ARGS
        )
    fi
    if [ -n "$KLUS_CMAKE_RUN_NINJA" ]
    then
        (
            set -x
            ninja
            ninja test
        )
    fi
}

cmake_klus_help ()
{
    cat << CMAKE_KLUS_HELP
$KLUS_USAGE_SCRIPT_PREAMBLE : configure a cmake build directory with some predefined defaults

This is a convenience wrapper script around cmake.
It should be run from within the (intended) build directory.
A valid source directory should be passed to this script as CMake argument.

Getting help:

 $KLUS_USAGE_SCRIPT_PREAMBLE -h|--help

Running CMake:

 $KLUS_USAGE_SCRIPT_PREAMBLE [options|cmake args]

Use -- to stop processing options and pass everything following -- as arguments to CMake:

 $KLUS_USAGE_SCRIPT_PREAMBLE [options|cmake args] -- [cmake args]
 $KLUS_USAGE_SCRIPT_PREAMBLE -- [cmake args]

Options:

 -a, --again       : (forcibly) wipe an existing CMake build directory and run CMake again
                     do not use with an "in source" build as that will destroy your checkout
 -r, --release     : omit sanitisers
 -t, --type <type> : override CMAKE_BUILD_TYPE (default is RelWithDebInfo)
 -k, --kde         : set CMAKE_INSTALL_PREFIX to KDE_DIST_DIR in klusrc
                     requires a $KLUS_SCRIPT_NAME setup beforehand
 -n, --ninja       : build & test with Ninja after running CMake
 -p, --prefix      : set CMAKE_PREFIX_PATH to KDE_DIST_DIR in klusrc
                     requires a $KLUS_SCRIPT_NAME setup beforehand

Predefined CMake defaults:

 -G Ninja
 -DCMAKE_BUILD_TYPE=RelWithDebInfo
 -DECM_ENABLE_SANITIZERS=address;leak;undefined

CMAKE_KLUS_HELP
}

klus ()
{
    case "$1" in
        -h|--help)
            cmake_klus_help
            exit 0
        ;;
        *)
            if scan_for_cmake_klus_opts "$@"
            then
                if [ -z "$KLUS_CMAKE_SOURCE_DIR" ]
                then
                    echo "No CMakeLists.txt was detected" >&2
                    echo "It looks like no source directory was specified in the CMake arguments" >&2
                    echo "Cowardly refusing to run cmake" >&2
                    exit 200
                fi

                try_cmake_klus
            else
                cmake_klus_help >&2
                exit 200
            fi
        ;;
    esac
}
