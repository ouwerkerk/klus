#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

klus_get_arcanist_mounts ()
{
    echo -n " -v \"$ARC_RC:$HOME/.arcrc\""
    echo -n " -v \"$SSH_HOSTS:$HOME/.ssh/known_hosts\""
    echo -n " -v \"$KLUS_SCRIPT_DIR/docker/arcanist.sh:/arcanist/wrapper.sh:ro\""
    echo -n " -v \"$KDE_IDENTITY:$HOME/.ssh/kde_identity:ro\""
    echo -n " -v \"$SSH_CONFIG:$HOME/.ssh/config:ro\""
    echo -n " -v \"$GIT_CONFIG:$HOME/.gitconfig:ro\""
}

klus ()
{
    . "$KLUS_SCRIPT_DIR/util/container-tool.sh"
    container_tool_klus arcanist klus_get_arcanist_mounts "$@"
}
