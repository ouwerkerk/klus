#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
#

KLUS_TEST_QUIET=""
KLUS_TEST_DIR=""
KLUS_TEST_PRINT_ROOT_DIR=""

scan_for_test_config_klus_opts ()
{
    while [ $# -ne 0 ]
    do
        case "$1" in
        -q|--quiet)
            if [ -z "$KLUS_TEST_QUIET" -a -z "$KLUS_TEST_PRINT_ROOT_DIR" ]
            then
                KLUS_TEST_QUIET="true"
            else
                echo "Duplicate/confliction option: $1" >&2
                return 1
            fi
        ;;
        -d|--directory)
            if [ -z "$KLUS_TEST_QUIET" -a -z "$KLUS_TEST_PRINT_ROOT_DIR" ]
            then
                KLUS_TEST_PRINT_ROOT_DIR="true"
            else
                echo "Duplicate/confliction option: $1" >&2
                return 1
            fi
        ;;
        *)
            if [ -d "$1" ]
            then
                if [ -z "$KLUS_TEST_DIR" ]
                then
                    KLUS_TEST_DIR="$1"
                else
                    echo "Too many directories: $1" >&2
                    return 1
                fi
            else
                echo "Invalid option/not a directory: $1" >&2
                return 1
            fi
        ;;
        esac
        shift 1
    done
}

test_klus_config_help ()
{
    cat << TEST_KLUS_HELP
$KLUS_USAGE_SCRIPT_PREAMBLE : inspect Klus configuration

Getting help:

 $KLUS_USAGE_SCRIPT_PREAMBLE -h|--help

Inspecting Klus directory:

 $KLUS_USAGE_SCRIPT_PREAMBLE [options]
 $KLUS_USAGE_SCRIPT_PREAMBLE [options] <directory>

If no directory is specified, the current working directory wil be used instead: $(pwd)

Options:

 -q, --quiet     : do not generate output, use exit status code to indicate whether the directory is a Klus environment
 -d, --directory : print path to the (root) directory of the Klus environment, if found

TEST_KLUS_HELP
}

klus_config_found ()
{
    local klusrc="$1"
    local klusdir="$2"

    if [ -n "$KLUS_TEST_PRINT_ROOT_DIR" ]
    then
        echo "$klusdir"
    elif [ -z "$KLUS_TEST_QUIET" ]
    then
        echo "Directory is part of a Klus environment: $KLUS_TEST_DIR"
        echo "Found klusrc in: $klusdir"
        echo
        cat "$klusrc"
        echo
    fi
}

klus_config_not_found ()
{
    local klusdir="$1"
    if [ -z "$KLUS_TEST_QUIET" -a -z "$KLUS_TEST_PRINT_ROOT_DIR" ]
    then
        echo "No klusrc was detected"
        echo "It looks like Klus is not yet set up for: $KLUS_TEST_DIR"
    fi
    exit 1
}

try_klus_config_test ()
{
    . "$KLUS_SCRIPT_DIR/util/find-klusrc.sh"

    if [ -z "$KLUS_TEST_DIR" ]
    then
        KLUS_TEST_DIR="$(pwd)"
    fi

    if [ -d "$KLUS_TEST_DIR" ]
    then
        cd "$KLUS_TEST_DIR"
    fi
    find_klus_rc klus_config_found klus_config_not_found
}

klus ()
{
    case "$1" in
        -h|--help)
            test_klus_config_help
            exit 0
        ;;
        *)
            if scan_for_test_config_klus_opts "$@"
            then
                try_klus_config_test
            else
                test_klus_config_help >&2
                exit 200
            fi
        ;;
    esac
}
