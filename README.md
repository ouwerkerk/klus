<!--
  - SPDX-License-Identifier: GPL-3.0-or-later
  - SPDX-FileCopyrightText: 2020 Johan Ouwerkerk <jm.ouwerkerk@gmail.com>
 -->
# Klus
Klus is a set of opinionated scripts for a KDE dev environment

## Description
Opinionated, in this context, means the following features...

### arcanist
Avoid an entire PHP stack on the host OS just to run a piece of arcane tooling: use a container instead.

### kdesrc-build
Avoid installing a sprawl of build-time dependencies: use a container instead.
Let `kdesrc-build` build as much as possible: include Qt.
Make sure to tweak some settings for better parallelisation: prefer `ninja` over `make` and configure `-j` settings.

### cmake
Typing is a chore: make it easy to configure against what `kdesrc-build` just built for you.
Sanity is not guaranteed: make sure to enable sanitisers.
Some other tweaks: prefer `ninja`, use release builds with debug info (should be the default).
Make it easy to do a full and clean rebuild: wipe directories and re-run cmake.

### flatpak-builder
No special privileges: make sure that all flatpak actions only affect your own flatpak user installation.
Intended for application developers:

 - Add necessary flatpak repositories to your user installation
 - Automatically install required dependencies when building a flatpak
 - Automatically install the flatpak after building it. That way you can immediately run your flatpak once it is built.
 - Enable automatic rebuilds if upstream Sdk bundles changed.

Flatpak does unconventional things and sometimes it breaks: provide escape hatches to wipe and start over.

### shell
Typing out environment variables for software compiled with `kdesrc-build` is tedious: make it easy to enter `$SHELL` with everything set up.

### apk
The KDE Android SDK is nice but long docker commands with volume mounts are hard to remember correctly.
Plus it is a lot easier if the resulting APK files are owned by the correct user right away.
Make it easy to do a full and clean rebuild: wipe directories.
Support building for only a single Android architecture: makes everything a lot faster.
Additionally some other tweaks: set appropriate `MAKEFLAGS`/`NINJAFLAGS` to get parallelism by default.

## Requirements
Klus relies on the following tools:

 - `git`
 - `cmake`
 - `ninja`
 - `flatpak` and `flatpak-builder`
 - You need to have a basic working shell environment

## Installing Klus

  - Clone: `git clone https://invent.kde.org/ouwerkerk/klus.git`
  - Symlink so it is on your `PATH`, e.g.: `ln -sT klus.sh ~/bin/klus.sh`

## Getting started with Klus

  - Run `klus.sh --help` to see what it can do for you

Happy hacking!
